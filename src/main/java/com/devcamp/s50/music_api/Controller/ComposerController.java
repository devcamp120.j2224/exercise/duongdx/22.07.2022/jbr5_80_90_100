package com.devcamp.s50.music_api.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.music_api.Service.ComposerService;
import com.devcamp.s50.music_api.model.*;


@RequestMapping("/")
@RestController
@CrossOrigin
public class ComposerController {
    @Autowired
    private ComposerService composerService ;
    @GetMapping("all-band")
    public ArrayList<Band> getAllBandApi(){
        ArrayList<Band> allBand = composerService.getAllBand();
        return allBand ;
    }

    @GetMapping("all-Artist")
    public ArrayList<Artist> getArtistsAPI(){
        ArrayList<Artist> all = composerService.getAllArtist();
        return all ;
    }

    @GetMapping("all")
    public ArrayList<Composer> getAll(){
        ArrayList<Composer> all = composerService.getComposerAll();
        return all ;
    }
}
