package com.devcamp.s50.music_api.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.s50.music_api.model.Album;

@Service
public class AlbumService {
    public ArrayList<Album> getlbums1(){
        List<String> List1 = new ArrayList<String>();
        List<String> List2 = new ArrayList<String>();
        List<String> List3 = new ArrayList<String>();
        List1.add("Anh Nho em, Anh Va Em, Anh Mat Em");
        List2.add("Cay , Hoa , La , Canh");
        List3.add("Buon , Vui , Suong");
        Album Anh = new Album("Anh", List1);
        Album Cay = new Album("Cay", List2);
        Album CamXuc = new Album("CamXuc", List3);
        ArrayList<Album> AllAlbum = new ArrayList<>();
        AllAlbum.add(Anh);
        AllAlbum.add(Cay);
        AllAlbum.add(CamXuc);
        return AllAlbum ;
    }
    public ArrayList<Album> getlbums2(){
        List<String> List4 = new ArrayList<String>();
        List<String> List5 = new ArrayList<String>();
        List<String> List6 = new ArrayList<String>();
        List4.add("Yeu, Ghet , Han ");
        List5.add("Xa , Gan , Ve ");
        List6.add("Nhà , Xe , Máy Bay ");
        Album Yeu = new Album("Yeu", List4);
        Album di = new Album("di", List5);
        Album TaiSan = new Album("TaiSan", List6);
        ArrayList<Album> AllAlbum = new ArrayList<>();
        AllAlbum.add(Yeu);
        AllAlbum.add(di);
        AllAlbum.add(TaiSan);
        return AllAlbum ;
    }
    public ArrayList<Album> getlbums3(){
        List<String> List7 = new ArrayList<String>();
        List<String> List8 = new ArrayList<String>();
        List<String> List9 = new ArrayList<String>();
        List<String> List10 = new ArrayList<String>();
        List7.add("Trường , Lớp , Bạn Học ");
        List8.add("Đi Đâu , Ăn Gì , Với Ai");
        List9.add("Cảm Ơn , Xin Lỗi , Chửi Thề ");
        List10.add("Rap Đi , Đen , 10 Năm ");
        Album Truong = new Album("Truong", List7);
        Album ThuVui = new Album("ThuVui", List8);
        Album ChuiThe = new Album("ChuiThe", List9);
        Album Rap = new Album("Rap", List10);
        ArrayList<Album> AllAlbum = new ArrayList<>();
        AllAlbum.add(Truong);
        AllAlbum.add(ThuVui);
        AllAlbum.add(ChuiThe);
        AllAlbum.add(Rap);
        return AllAlbum ;
    }
    public ArrayList<Album> getAlllbums(){
        List<String> List1 = new ArrayList<String>();
        List<String> List2 = new ArrayList<String>();
        List<String> List3 = new ArrayList<String>();
        List<String> List4= new ArrayList<String>();
        List<String> List5 = new ArrayList<String>();
        List<String> List6= new ArrayList<String>();
        List<String> List7 = new ArrayList<String>();
        List<String> List8 = new ArrayList<String>();
        List<String> List9 = new ArrayList<String>();
        List<String> List10 = new ArrayList<String>();
        List1.add("Anh Nho em, Anh Va Em, Anh Mat Em");
        List2.add("Cay , Hoa , La , Canh");
        List3.add("Buon , Vui , Suong");
        List4.add("Yeu, Ghet , Han ");
        List5.add("Xa , Gan , Ve ");
        List6.add("Nhà , Xe , Máy Bay ");
        List7.add("Trường , Lớp , Bạn Học ");
        List8.add("Đi Đâu , Ăn Gì , Với Ai");
        List9.add("Cảm Ơn , Xin Lỗi , Chửi Thề ");
        List10.add("Rap Đi , Đen , 10 Năm ");
        
        Album Anh = new Album("Anh", List1);
        Album Cay = new Album("Cay", List2);
        Album CamXuc = new Album("CamXuc", List3);
        Album Yeu = new Album("Yeu", List4);
        Album di = new Album("di", List5);
        Album TaiSan = new Album("TaiSan", List6);
        Album Truong = new Album("Truong", List7);
        Album ThuVui = new Album("ThuVui", List8);
        Album ChuiThe = new Album("ChuiThe", List9);
        Album Rap = new Album("Rap", List10);
        ArrayList<Album> AllAlbum = new ArrayList<>();
        AllAlbum.add(Anh);
        AllAlbum.add(Cay);
        AllAlbum.add(CamXuc);
        AllAlbum.add(Yeu);
        AllAlbum.add(di);
        AllAlbum.add(TaiSan);
        AllAlbum.add(Truong);
        AllAlbum.add(ThuVui);
        AllAlbum.add(ChuiThe);
        AllAlbum.add(Rap);

        return AllAlbum ;
    }
   
}
