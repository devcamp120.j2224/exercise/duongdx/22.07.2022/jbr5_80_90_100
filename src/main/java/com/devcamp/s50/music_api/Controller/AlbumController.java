package com.devcamp.s50.music_api.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.music_api.Service.AlbumService;
import com.devcamp.s50.music_api.model.Album;


@RequestMapping("/")
@RestController
@CrossOrigin
public class AlbumController {
    @Autowired
    private AlbumService albumService ;

    @GetMapping("all-Album")
    public ArrayList<Album> getAlllbums(){
        ArrayList<Album> allAlbum = albumService.getAlllbums();
        return allAlbum ;
    }

}
