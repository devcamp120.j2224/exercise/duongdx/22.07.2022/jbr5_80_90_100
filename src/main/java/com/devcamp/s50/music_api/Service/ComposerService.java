package com.devcamp.s50.music_api.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.music_api.model.Artist;
import com.devcamp.s50.music_api.model.Band;
import com.devcamp.s50.music_api.model.BandMember;
import com.devcamp.s50.music_api.model.Composer;

@Service
public class ComposerService {
    @Autowired
    private AlbumService albumService ;
        Artist Duong = new Artist("Dao", "Duong", "Đá");
        Artist Linh = new Artist("Linh", "Thuy", "Bé");
        Artist artist1 = new Artist("Hưng", "Tuấn", "Tuấn Hưng");

        BandMember nguoi1 = new BandMember("Den", "Vau", "DenVau", "Raper");
        BandMember nguoi2 = new BandMember("Bin", "Z", "Binz", "Raper");
        BandMember nguoi3 = new BandMember("Dam", "Vinh Hunh", "Dam", "Ca Sĩ");
        BandMember nguoi4 = new BandMember("Linh", "Chế", "Chế Linh", "Ca Sĩ");
        BandMember nguoi5 = new BandMember("Ngọc", "Tuấn", "Tuấn Ngọc", "DanhCa");
        BandMember nguoi6 = new BandMember("Tí", "Cu", "Tí", "Thổi Kèn");
    public ArrayList<Artist> getAllArtist(){
        Duong.setAlbums(albumService.getlbums1());
        Linh.setAlbums(albumService.getlbums2());
        artist1.setAlbums(albumService.getlbums3());
        ArrayList<Artist> all = new ArrayList<>();
        all.add(Duong);
        all.add(Linh);
        all.add(artist1);
        return all ;
    }

    public ArrayList<Composer> getComposerAll(){
        

        Duong.setAlbums(albumService.getlbums1());
        Linh.setAlbums(albumService.getlbums2());
        artist1.setAlbums(albumService.getlbums3());

        ArrayList<Composer> allComposer = new ArrayList<>();
        allComposer.add(Duong);
        allComposer.add(Linh);
        allComposer.add(artist1);
        allComposer.add(nguoi1);
        allComposer.add(nguoi2);
        allComposer.add(nguoi3);
        allComposer.add(nguoi4);
        allComposer.add(nguoi5);
        allComposer.add(nguoi6);

        return allComposer ;
    }  

    public ArrayList<Band> getAllBand(){
        ArrayList<BandMember> nhom1 = new ArrayList<>();
        nhom1.add(nguoi1);
        nhom1.add(nguoi2);
        nhom1.add(nguoi3);
        ArrayList<BandMember> nhom2 = new ArrayList<>();
        nhom2.add(nguoi4);
        nhom2.add(nguoi5);
        nhom2.add(nguoi6);
        Band band1 = new Band("Ê Ke",nhom1, albumService.getlbums1());
        Band band2 = new Band("DuaHau",nhom2, albumService.getlbums2());

        ArrayList<Band> AllBand = new ArrayList<>();
        AllBand.add(band1);
        AllBand.add(band2);
        return AllBand ;
    }
}
