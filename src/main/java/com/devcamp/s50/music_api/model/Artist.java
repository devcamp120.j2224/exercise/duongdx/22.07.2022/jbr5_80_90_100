package com.devcamp.s50.music_api.model;

import java.util.List;

public class Artist extends Composer{
    private List<Album> Albums;

    public Artist(String firstname, String lastname, String stagename, List<Album> albums) {
        super(firstname, lastname, stagename);
        Albums = albums;
    }

    public Artist(String firstname, String lastname, String stagename) {
        super(firstname, lastname, stagename);
    }

    public List<Album> getAlbums() {
        return Albums;
    }

    public void setAlbums(List<Album> albums) {
        Albums = albums;
    }

    @Override
    public String toString() {
        return "Artist [Albums=" + Albums + "]";
    }
    
}
